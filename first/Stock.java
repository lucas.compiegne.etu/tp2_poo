
/**
 * Write a description of class Stock here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Stock
{
    private int quantity;
    
    /*
     * Constructeur pour un stock de 0.
     */
    public Stock(){
        this.quantity = 0;
    }
    
    /*
     * Constructeur avec un stock défini.
     */
    public Stock(int n){
        this.quantity = n;
    }
    /*
     * Méthode qui permet de récupérer la quantité de marchandise en stock.
     * @return la quantité de marchandise en stock.
     */
    public int getQuantity(){
        return this.quantity;
    }
    
    /*
     * Méthode qui permet d'ajouter du stock.
     */
    public void add(int n){
        this.quantity += n;
    }
    
    /*
     * Méthode qui permet de retirer du stock.
     */
    public void remove(int n){
        if(n > this.quantity){
            this.quantity = 0;
        }
        else{
            this.quantity -= n;
        }
    }
    
    /*
     * Permet d'afficher le stock.
     */
    public String toString(){
        return "the stock's quantity is :" + this.quantity;
    }
}

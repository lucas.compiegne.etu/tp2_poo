public class Lightbulb
{
    private int Power;
    private String Color;
    private boolean state;
    
    /*
     * Constructeur qui prend en paramètre Power et la couleur
     */
    public Lightbulb(int p, String c){
        this.Power = p;
        this.Color = c;
    }
    
    /*
     * Constructeur qui prend en paramètre Power et met la couleur à blanc
     */
    public Lightbulb(int p){
        this.Power = p;
        this.Color = "white";
    }
    
    /*
     * Méthode qui return le power
     */
    public int getPower(){
        return this.Power;
    }
    
    /*
     * Méthode qui return la couleur
     */
    public String getColor(){
        return this.Color;
    }
    
    /*
     * Méthode qui return l'état de la lampe
     */
    public boolean isOn(){
        return this.state;
    }
    
    /*
     * Méthode qui allume la lampe
     */
    public void on(){
        this.state = true;
    }
    
    /*
     * Méthode qui éteint la lampe
     */
    public void off(){
        this.state = false;
    }
    
    /*
     * Méthode qui décrit l'ampoule
     */
    public String toString(){
        return "L'ampoule consomme "+this.Power+" est de couleur "+this.Color+" et est actuellement allumé : "+this.state;
    }
} 
   

/**
 * Write a description of class Goods here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Goods
{
    private double price;
    private String name;
    
    /*
     * Constructeur qui initialise Goods avec price à 0 et name selon user   
     */
    public Goods(String n){
        this.name = n;
        this.price = 0;
    }
    
    /*
     * Constructeur qui initialise Goods avec price et name selon user   
     */
    public Goods(String n, double p){
        this.name = n;
        this.price = p;
    }
    
    /*
     * getName pour avoir name
     */
    
    public String getName(){
        return this.name;
    }
    /*
     * getPrice pour avoir  price
     */
    public double getPrice(){
        return this.price;
    }
    
    /*
     * setPrice pour modifier le price
     */
    public void setPrice(float newPrice){
        this.price = newPrice;
    }
    
    /*
     * Permet d'afficher name et price
     */
    public String toString(){
        return "the goods "+this.name+" costs "+this.price;
    } 
    
    public double priceTVA(){
        double a = (this.price*20)/100;
        this.price += a;
        return this.price;
    }
}
